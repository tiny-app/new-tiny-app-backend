<?php

use Illuminate\Database\Seeder;
use App\Modules\Finance\Models\TransactionType;

class TransactionTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TransactionType::truncate();
        $data = [
            ['name' => 'Pemasukan', 'category' => 1, 'is_pending' => 0],
            ['name' => 'Pengeluaran', 'category' => 2, 'is_pending' => 0],
            ['name' => 'Target Pemasukan', 'category' => 1, 'is_pending' => 1],
            ['name' => 'Target Pengeluaran', 'category' => 2, 'is_pending' => 1],
        ];

        TransactionType::insert($data);
    }
}
