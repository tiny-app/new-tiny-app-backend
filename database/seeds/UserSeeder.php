<?php

use Illuminate\Database\Seeder;
use App\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();
        // Admin => ID = 1
        $data = [
            [
                'name' => 'Difa',
                'email' => 'difacool40@gmail.com',
                'username' => 'difaananda40',
                'password' => bcrypt('123321'),
            ],
            [
                'name' => 'Rois',
                'email' => 'rois@gmail.com',
                'username' => 'rois',
                'password' => bcrypt('123321'),
            ],
            [
                'name' => 'Julian',
                'email' => 'joel@gmail.com',
                'username' => 'joel',
                'password' => bcrypt('123321'),
            ]
        ];

        $super = User::insert($data);
    }
}
