<?php

use Illuminate\Database\Seeder;
use App\Modules\Profile\Models\ProjectFiles;

class ProjectFilesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ProjectFiles::truncate();
        $data = [
            ['file' => 'file 1', 'project_id' => 1, 'disk' => 'local', 'file_url' => 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRzhcKErHhU-LkFu9CHQJ0eL8eoIq23INGj2ngWBjhjfxxPabMp'],
            ['file' => 'file 2', 'project_id' => 2, 'disk' => 'gdrive', 'file_url' => 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRzhcKErHhU-LkFu9CHQJ0eL8eoIq23INGj2ngWBjhjfxxPabMp'],
        ];

        ProjectFiles::insert($data);
    }
}
