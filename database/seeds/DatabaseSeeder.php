<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        
        // Transaction
        $this->call(UserSeeder::class);
        $this->call(TransactionTypeSeeder::class);
        $this->call(TransactionSeeder::class);

        // Profile
        $this->call(ProjectAdditionalsSeeder::class);
        $this->call(ProjectsSeeder::class);
        $this->call(ProjectTechnologiesSeeder::class);
        $this->call(ProjectFilesSeeder::class);
        
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
