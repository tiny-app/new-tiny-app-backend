<?php

use Illuminate\Database\Seeder;
use App\Modules\Profile\Models\Projects;

class ProjectsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Projects::truncate();
        $data = [
            [
                'title' => 'Keuangan', 
                'description' => 1,
                'thumbnail'     => 'foto 1', 
                'thumbnail_url' => 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRzhcKErHhU-LkFu9CHQJ0eL8eoIq23INGj2ngWBjhjfxxPabMp',
                'video_link' => 'https://www.youtube.com/watch?v=2Pgww17x3pw', 
                'demo_link' => 'http://tinystudioo.com/',
                'disk'      => 'local'
            ],
            [
                'title' => 'Web Profile', 
                'description' => 2,
                'thumbnail'     => 'foto 2',
                'thumbnail_url' => 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRzhcKErHhU-LkFu9CHQJ0eL8eoIq23INGj2ngWBjhjfxxPabMp', 
                'video_link' => 'https://www.youtube.com/watch?v=2Pgww17x3pw', 
                'demo_link' => 'http://tinystudioo.com/',
                'disk'      => 'gdrive'
            ],
        ];

        Projects::insert($data);
    }
}
