<?php

use Illuminate\Database\Seeder;
use App\Modules\Profile\Models\ProjectTechnologies;

class ProjectTechnologiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ProjectTechnologies::truncate();
        $data = [
            ['project_id' => 1, 'project_additional_id' => 3],
            ['project_id' => 1, 'project_additional_id' => 1],
            ['project_id' => 2, 'project_additional_id' => 4],
            ['project_id' => 2, 'project_additional_id' => 2],
        ];

        ProjectTechnologies::insert($data);
    }
}
