<?php

use Illuminate\Database\Seeder;
use App\Modules\Profile\Models\ProjectAdditionals;

class ProjectAdditionalsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ProjectAdditionals::truncate();
        $data = [
            ['name' => 'PHP', 'type' => 1],
            ['name' => 'Javascript', 'type' => 1],
            ['name' => 'Laravel', 'type' => 2],
            ['name' => 'React JS', 'type' => 2],
        ];

        ProjectAdditionals::insert($data);
    }
}
