<?php

// Index API
Route::get('/', function () {
    return response()->json(['code' => 200, 'message' => 'Tiny App API']);
});

// Passport Auth
Route::post('login', 'AuthController@login')->name('login');
Route::get('unauthorized', 'AuthController@unauthorized')->name('unauthorized');

// Fallback 404 route
Route::fallback(function() {
    return response()->json(['code' => 404, 'message' => 'Tidak Ditemukan'], 404);
})->name('api.fallback.404');

Route::get('storage_generate/finance/{file}', 'StorageController@file_generate')->name('storage.url.finance');
Route::get('storage_generate/project/{file}', 'StorageController@file_generate')->name('storage.url.project');


Route::group(['middleware' => ['auth:api']], function() {
    Route::get('is_login', function () {
        return response()->json([
            'code' => 200,
            'message' => 'Logged in',
            ]);
        });
        
    Route::post('logout', 'AuthController@logout');
        
});
    