<?php

namespace App\Observers;

use App\Service\Helpers;
use Illuminate\Http\Request;
use App\Modules\Profile\Models\Projects;
use App\Modules\Profile\Models\ProjectTechnologies;

class ProjectObserver
{
    public function __construct(Request $request)
    {
      $this->request = $request;
    }
    /**
     * Handle the project "created" event.
     *
     * @param  \App\project  $project
     * @return void
     */
    public function created(Projects $project)
    {
      $this->createAdditionalData($project);
    }

    /**
     * Handle the project "updated" event.
     *
     * @param  \App\project  $project
     * @return void
     */
    public function updated(Projects $project)
    {
      $this->deleteAdditionalData($project);
      $this->createAdditionalData($project);
    }

    /**
     * Handle the project "deleting" event.deleted => after | deleting => before
     *
     * @param  \App\project  $project
     * @return void
     */
    public function deleting(Projects $project)
    {
      $this->deleteAdditionalData($project);
      $this->deleteFiles($project);
    }

    protected function createAdditionalData($project)
    {
      $technologies = json_decode($this->request['technologies'], true);

      if ($technologies) {
        $technologies_data = $this->setTechnologies($technologies);
        $project->technologies()->saveMany($technologies_data);
      }
    }

    protected function deleteAdditionalData($project)
    { 
      if ($project->technologies()->count() > 0) {
        $project->technologies()->delete();
      }
    }

    protected function deleteFiles($project)
    {
      if ($project->files()->count() > 0) {
        foreach ($project->files as $file) {
          Helpers::delete_file('project', $file->file, $file->disk);
          $file->delete();
        }
      }
    }

    protected function setTechnologies($technologies)
    {
        $technologies_data = array();
        foreach ($technologies as $technology) {
          $technologies_data[] = new ProjectTechnologies([
            'project_additional_id' => $technology,
          ]);
        }
        
        return $technologies_data;
    }
}
