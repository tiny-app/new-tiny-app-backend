<?php

namespace App\Service;

use Storage;

class Helpers {
  public static function upload_handler($img, $folder, $oldData = null, $customFile = null) {
    if ($oldData !== null) {
      if ($customFile === null) {
        $oldDataFile = $oldData->file;
      } else {
        $oldDataFile = $customFile;
      }
      self::delete_file($folder, $oldDataFile, $oldData->disk);
    }

    if (env('STORAGE_DISK') === 'gdrive') {
      $dir = config('app.config.gdrive_folder.' . $folder);
      $imgName = uniqid() . '_' . $img->getClientOriginalName();
      Storage::disk('google')->put($dir . $imgName, fopen($img, 'r+'));
      $url = Storage::disk('google')->url($dir . $imgName);

      return [
        'name' => $imgName, 
        'url' => $url
      ];
    } elseif (env('STORAGE_DISK') === 'local') {
      $path = public_path('storage/' . $folder . '/');
      $photo = uniqid() . '_' . $img->getClientOriginalName();
      $img->move($path, $photo);

      return [
        'name' => $photo, 
        'url' => route('storage.url.'.$folder, $photo)
      ];
    }

    return null;
  }

  public static function delete_file($folder, $file, $disk) {
    if ($disk === 'local') {
      \File::delete(public_path('storage/' . $folder . '/' . $file));
    }
  }
}