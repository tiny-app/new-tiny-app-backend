<?php

// Profile => Project
Route::apiResource('project', 'ProjectController')->only('index', 'show');
Route::get('project/files/{projectId}', 'ProjectFilesController@index')->name('project.files.index');
Route::get('project/files/{projectId}', 'ProjectFilesController@index')->name('project.files.index');

Route::group(['middleware' => ['auth:api']], function() {
    // Profile => Project
    Route::apiResource('project', 'ProjectController')->except('index', 'show');

    // Profile => Project
    Route::apiResource('project/files', 'ProjectFilesController')->only('update', 'destroy');
    Route::post('project/files/{projectId}', 'ProjectFilesController@store')->name('project.files.store');

    // Profile => Project Addinionals
    Route::apiResource('project_additionals', 'ProjectAdditionalsController');
});