<?php

namespace App\Modules\Profile\Models;

use Illuminate\Database\Eloquent\Model;

class ProjectTechnologies extends Model
{
    protected $table = 'project_technologies';
    protected $fillable = [
        'project_id',
        'project_additional_id',
    ];

    protected $hidden = [
        'created_at', 'updated_at'
    ];

    public $timestamps = true;

    public function project() {
        return $this->belongsTo(Projects::class, 'project_id');
    }

    public function project_additional() {
        return $this->belongsTo(ProjectAdditionals::class, 'project_additional_id');
    }
}
