<?php

namespace App\Modules\Profile\Models;

use Illuminate\Database\Eloquent\Model;

class ProjectAdditionals extends Model
{
    protected $table = 'project_additionals';
    protected $fillable = [
      'name',
      'type'
    ];

    protected $hidden = [
      'created_at', 'updated_at'
    ];

    protected $appends = ['type_formatted'];

    public $timestamps = true;

    public function projects_technologies() {
      return $this->hasMany(ProjectTechnologies::class, 'project_additional_id');
    }

    public function countProjectTechbologiesRelation()
    {
      return $this->projects_technologies()->count();
    }

    public function scopeFilter($query, $request)
    {
      if ($request->has('name')) {
          $query->where('name', 'like', '%' . $request->name . '%');
      }

      if ($request->has('type')) {
        $query->where('type', $request->type);
    }

      return $query;
    }

    public function getTypeFormattedAttribute()
    {
      $type = (int) $this->type;
      if ($type === 1) {
        $type = 'Language';
      } elseif ($type === 2) {
        $type = 'Framework';
      } else {
        $type = 'Any';
      }

      return $type;
    }
}
