<?php

namespace App\Modules\Profile\Models;

use Illuminate\Database\Eloquent\Model;

class ProjectFiles extends Model
{
    protected $table = 'project_files';
    protected $fillable = [
        'file',
        'file_url',
        'disk',
        'project_id'
    ];

    protected $hidden = [
        'created_at', 'updated_at'
    ];

    public $timestamps = true;

    public function project() {
        return $this->belongsTo(Projects::class, 'project_id');
    }
}
