<?php

namespace App\Modules\Profile\Models;

use Illuminate\Database\Eloquent\Model;

class Projects extends Model
{
    protected $table = 'projects';
    protected $fillable = [
        'title',
        'description',
        'thumbnail',
        'thumbnail_url',
        'video_link',
        'demo_link',
        'disk'
    ];

    protected $hidden = [
        'created_at', 'updated_at'
    ];

    public $timestamps = true;

    public function files() {
        return $this->hasMany(ProjectFiles::class, 'project_id');
    }

    public function technologies() {
        return $this->hasMany(ProjectTechnologies::class, 'project_id');
    }

    public function languages() {
        return $this->hasMany(ProjectTechnologies::class, 'project_id')->whereHas('project_additional', function($q) {
            $q->where('type', 1);
        })->with('project_additional');
    }

    public function frameworks() {
        return $this->hasMany(ProjectTechnologies::class, 'project_id')->whereHas('project_additional', function($q) {
            $q->where('type', 2);
        })->with('project_additional');
    }

    public function scopeFilter($query, $request)
    {
      if ($request->has('title')) {
          $query->where('title', 'like', '%' . $request->title . '%');
      }

      return $query;
    }
}
