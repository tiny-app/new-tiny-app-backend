<?php

namespace App\Modules\Profile\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use App\Service\ApiResponse;
use App\Http\Controllers\Controller;
use App\Http\Resources\ProjectAdditionalsCollection;
use App\Modules\Profile\Models\ProjectAdditionals;
use App\Modules\Profile\Http\Requests\ProjectAdditionalsRequest;

class ProjectAdditionalsController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $page = $request->per_page ?? 10;
        $data = ProjectAdditionals::filter($request)->orderBy('name', 'ASC')->paginate($page);

        return ApiResponse::success(new ProjectAdditionalsCollection($data));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProjectAdditionalsRequest $request)
    {
        DB::beginTransaction();
        try {
            $create = ProjectAdditionals::create($request->all());
            DB::commit();
            
            return ApiResponse::store($create);
        } catch (\Throwable $e) {
            DB::rollback();
            return ApiResponse::error($e->getMessage(), $e->getCode());
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProjectAdditionalsRequest $request, $id)
    {
        DB::beginTransaction();
        try {
            $data = ProjectAdditionals::find($id);
            if ($data == null) { return ApiResponse::error(); }

            $data->update($request->all());
            DB::commit();
    
            return ApiResponse::update($data);
        } catch (\Throwable $e) {
            DB::rollback();
            return ApiResponse::error($e->getMessage(), $e->getCode());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $data = ProjectAdditionals::find($id);
            if ($data == null) { return ApiResponse::error(); }
            if ($data->countProjectTechbologiesRelation() > 0) {
                return ApiResponse::error_relation();
            }
            
            $data->delete();

            DB::commit();
            return ApiResponse::delete();
        } catch (\Throwable $e) {
            DB::rollback();
            return ApiResponse::error($e->getMessage(), $e->getCode());
        }
    }
}
