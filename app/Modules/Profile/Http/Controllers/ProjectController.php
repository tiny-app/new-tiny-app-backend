<?php

namespace App\Modules\Profile\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use App\Service\Helpers;
use App\Service\ApiResponse;
use App\Http\Controllers\Controller;
use App\Modules\Profile\Models\Projects;
use App\Http\Resources\ProjectCollection;
use App\Modules\Profile\Http\Requests\ProjectRequest;

class ProjectController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $page = $request->per_page ?? 10;
        $data = Projects::filter($request)->paginate($page);
        
        return ApiResponse::success(new ProjectCollection($data));
    }

     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $page = $request->per_page ?? 10;
        $data = Projects::where('id', $id)->with('files', 'frameworks', 'languages')->first();
        $data = $this->reduceData($data);

        return ApiResponse::success($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProjectRequest $request)
    {
        DB::beginTransaction();
        try {
            if (isset($request->thumbnail_name)) {
                $img = Helpers::upload_handler($request->file('thumbnail_name'), 'project');
                $request->merge([
                  'thumbnail' => $img['name'],
                  'thumbnail_url' => $img['url'],
                  'disk'        => env('STORAGE_DISK')
                ]);
            }
            
            $create = Projects::create($request->all());
            DB::commit();
            
            return ApiResponse::store($create);
        } catch (\Throwable $e) {
            DB::rollback();
            dd($e->getMessage());
            return ApiResponse::error($e->getMessage(), $e->getCode());
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProjectRequest $request, $id)
    {
        DB::beginTransaction();
        try {
            $data = Projects::find($id);
            if ($data == null) { return ApiResponse::error(); }

            if (isset($request->thumbnail_name)) {
                $img = Helpers::upload_handler($request->file('thumbnail_name'), 'project', $data, $data->thumbnail);
                $request->merge([
                'thumbnail' => $img['name'],
                'thumbnail_url' => $img['url'],
                'disk'        => env('STORAGE_DISK')
                ]);
            }

            $data->update($request->all());
            DB::commit();
    
            return ApiResponse::update($data);
        } catch (\Throwable $e) {
            dd($e->getMessage());
            DB::rollback();
            return ApiResponse::error($e->getMessage(), $e->getCode());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $data = Projects::find($id);
            if ($data == null) { return ApiResponse::error(); }

            Helpers::delete_file('project', $data->thumbnail, $data->disk);
            
            $data->delete();

            DB::commit();
            return ApiResponse::delete();
        } catch (\Throwable $e) {
            dd($e->getMessage());
            DB::rollback();
            return ApiResponse::error($e->getMessage(), $e->getCode());
        }
    }

    private function reduceData ($data) {
        $frameworks = $data->frameworks->reduce(function ($output, $carry) {
            $output[] = [
                'id' => $carry->id,
                'name' => $carry->project_additional->name,
                'project_additional_id' => $carry->project_additional_id,
            ];

            return $output;
        }) ?? [];

        $languages = $data->languages->reduce(function ($output, $carry) {
            $output[] = [
                'id' => $carry->id,
                'name' => $carry->project_additional->name,
                'project_additional_id' => $carry->project_additional_id,
            ];

            return $output;
        }) ?? [];

        $data =  [
            'id'               => $data->id,
            'title'            => $data->title,
            'description'      => $data->description,
            'thumbnail'        => $data->thumbnail,
            'thumbnail_url'    => $data->thumbnail_url,
            'video_link'       => $data->video_link,
            'demo_link'        => $data->demo_link,
            'files'            => $data->files,
            'frameworks'       => $frameworks,
            'languages'        => $languages
        ];

        return $data;
    }
}
