<?php

namespace App\Modules\Profile\Http\Controllers;

use Illuminate\Http\Request;

use DB;
use App\Service\Helpers;
use App\Service\ApiResponse;
use App\Http\Controllers\Controller;
use App\Modules\Profile\Models\Projects;
use App\Modules\Profile\Models\ProjectFiles;
use App\Modules\Profile\Http\Requests\ProjectFilesRequest;

class ProjectFilesController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
			$data = ProjectFiles::where('project_id', $id)->get();

			return ApiResponse::success($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProjectFilesRequest $request, $id)
    {
			DB::beginTransaction();
			try {
				$project = Projects::find($id);
				if ($project == null) { return ApiResponse::error(); }

				$img = Helpers::upload_handler($request->file('image'), 'project');
				$request->merge([
					'file' => $img['name'],
					'disk' => env('STORAGE_DISK'), 
					'file_url' => $img['url'],
					'project_id' => $project->id
				]);

				$create = ProjectFiles::create($request->all());
				DB::commit();
				
				return ApiResponse::store($create);
			} catch (\Throwable $e) {
				DB::rollback();
				return ApiResponse::error($e->getMessage(), $e->getCode());
			}
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProjectFilesRequest $request, $id)
    {
			DB::beginTransaction();
			try {
					$data = ProjectFiles::find($id);
					if ($data == null) { return ApiResponse::error(); }

					$img = Helpers::upload_handler($request->file('image'), 'project', $data);
					$request->merge([
						'file' => $img['name'],
						'disk' => env('STORAGE_DISK'), 
						'file_url' => $img['url']
					]);
						
					$data->update($request->all());
					DB::commit();
	
					return ApiResponse::update($data);
			} catch (\Throwable $e) {
					DB::rollback();
					return ApiResponse::error($e->getMessage(), $e->getCode());
			}
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
			DB::beginTransaction();
			try {
					$data = ProjectFiles::find($id);
					if ($data == null) { return ApiResponse::error(); }
					
					Helpers::delete_file('project', $data->file, $data->disk);
					$data->delete();

					DB::commit();
					return ApiResponse::delete();
			} catch (\Throwable $e) {
					DB::rollback();
					return ApiResponse::error($e->getMessage(), $e->getCode());
			}
    }
}
