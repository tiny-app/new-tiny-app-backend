<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTypeProjectAdditional extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasColumn('project_additionals', 'type')){
            DB::statement("ALTER TABLE project_additionals CHANGE COLUMN type type ENUM('1', '2', '3') NOT NULL comment '[1]: Language, [2]: Framework, [3]: Any'");
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('project_additionals', 'type')){
            Schema::table('project_additionals', function (Blueprint $table) {
                $table->enum('type', [1, 2, 3])->comment('[1]: Language, [2]: Framework, [3]: Any');
            });
        }
    }
}
