<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjects extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->text('description')->nullable();
            $table->text('thumbnail')->nullable();
            $table->text('thumbnail_url')->nullable();
            $table->text('video_link')->nullable();
            $table->text('demo_link')->nullable();
            $table->enum('disk', ['local', 'gdrive'])->comment('[local]: Public Storage, [gdrive]: Google Drive Storage');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
