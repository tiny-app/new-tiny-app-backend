<?php

namespace App\Modules\Finance\Providers;

use Caffeinated\Modules\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the module services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(module_path('finance', 'Resources/Lang', 'app'), 'finance');
        $this->loadViewsFrom(module_path('finance', 'Resources/Views', 'app'), 'finance');
        $this->loadMigrationsFrom(module_path('finance', 'Database/Migrations', 'app'));
        if(!$this->app->configurationIsCached()) {
            $this->loadConfigsFrom(module_path('finance', 'Config', 'app'));
        }
        $this->loadFactoriesFrom(module_path('finance', 'Database/Factories', 'app'));
    }

    /**
     * Register the module services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }
}
