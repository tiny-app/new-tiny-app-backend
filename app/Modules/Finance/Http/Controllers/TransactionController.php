<?php

namespace App\Modules\Finance\Http\Controllers;

use DB;
use Storage;
use App\Models\User;
use App\Service\Helpers;
use Illuminate\Support\Str;
use App\Service\ApiResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\ValidationRequest;
use App\Http\Requests\TransactionRequest;
use App\Modules\Finance\Models\Transaction;
use App\Http\Resources\TransactionCollection;
use App\Modules\Finance\Models\TransactionType;

class TransactionController extends Controller
{
     /**
     * Fetch Data, Pending Menu => GET Pending
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $page = $request->per_page ?? 10;
        $data = Transaction::filter($request)->with('type', 'user');
        if ($request->has('pending')) {
            $data = $data->where('is_active', 0);
        } else {
            $data = $data->where('is_active', 1);
        }

        $data = $data->paginate($page);

        return ApiResponse::success(new TransactionCollection($data));
    }

    /**
     * Store Data, Pending Menu => GET Pending
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TransactionRequest $request)
    {
        DB::beginTransaction();
        try {
            $request->merge([
                'no_transaction' => $this->getLastNoTransaction(),
                'user_id'        => auth()->user()->id,
                'is_active'      => !$request->has('pending')
            ]);

            if ($request->image) {
              $img = Helpers::upload_handler($request->file('image'), 'project');
              $request->merge([
                'file' => $img['name'],
                'disk' => env('STORAGE_DISK'), 
                'file_url' => $img['url']
              ]);
            }

            $create = Transaction::create($request->all());
            $this->updateAmountType($request, $create);
            
            DB::commit();

            return ApiResponse::store($create);
        } catch (\Throwable $e) {
            dd($e->getMessage());
            DB::rollback();
            return ApiResponse::error($e->getMessage(), $e->getCode());
        }

    }

    /**
     * Update Data, Pending Menu => GET Pending
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(TransactionRequest $request, $id)
    {
        DB::beginTransaction();
        try {
            $data = Transaction::find($id);
            if ($data == null) { return ApiResponse::error(); }

            // Cari Cara ngecek dia ada atau tidak lalu hapus di gdrive
            if ($request->image) {
              $img = Helpers::upload_handler($request->file('image'), 'project', $data);
              $request->merge([
                'file' => $img['name'],
                'disk' => env('STORAGE_DISK'), 
                'file_url' => $img['url']
              ]);
            }
            
            $data = $this->updated($data, $request, $id);
            DB::commit();

            return ApiResponse::update($data);

        } catch (\Throwable $e) {
            dd($e->getMessage());
            DB::rollback();
            return ApiResponse::error($e->getMessage(), $e->getCode());
        }
    }

    public function done(ValidationRequest $request, $id)
    {
        DB::beginTransaction();
        try {
            $data = Transaction::find($id);
            if ($data == null) { return ApiResponse::error(); }

            if ($request->image) {
                $img = Helpers::upload_handler($request->file('image'), 'finance', $data);
                $request->merge([
                    'file' => $img['name'],
                    'disk' => env('STORAGE_DISK'), 
                    'file_url' => $img['url']
                ]);
            }
            
            $data = $this->updated($data, $request, $id, "merged");

            DB::commit();

            return ApiResponse::update($data);
        } catch (\Throwable $e) {
            DB::rollback();
            return ApiResponse::error($e->getMessage(), $e->getCode());
        }
    }

    /**
     * Delete And Cancel Transaction
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try {
            $data = Transaction::find($id);
            if ($data == null) { return ApiResponse::error(); }

            Helpers::delete_file('project', $data->file, $data->disk);
    
            $data->delete();
            DB::commit();
            
            return ApiResponse::delete();
        } catch (\Throwable $e) {
            DB::rollback();
        return ApiResponse::error($e->getMessage(), $e->getCode());
        }
    }

    public function sum_amount() {
        $data = collect();
        $transType = TransactionType::with('transactions')->get();
        foreach ($transType as $value) {
            $data->put(Str::snake($value->name), (float)str_replace('-', '', $value->sum_transaction));
        }

        $amount = Transaction::orderBy('updated_at', 'DESC')->where('is_active', true)->sum('amount');
        $data = $data->put(Str::snake('Balance'), $amount);
        return ApiResponse::success($data);
    }

    public function lastOrderNumber()
    {
        return ApiResponse::success([
            'no_transaction' => $this->getLastNoTransaction()
        ]);
    }

    private function updated($data, $request, $id, $merged = null) {
        if ($merged !== null) {
            $request->merge(['is_active' => true]);
        }
        
        $data->update($request->all());
        $this->updateAmountType($request, $data);

        return $data;
        
    }

    private function updateAmountType($request, $data) {
        if (isset($request->amount)) {
            if ($data->type->category === "2") {
                $data->update(['amount' => -$data->amount]);
            }
        }
    }

    public function getLastNoTransaction()
    {
        $last_data = Transaction::orderBy('id', 'desc')->first();
        if ($last_data !== null) {
            $last_no_transaction = $this->splitNoTransaction($last_data->no_transaction);
        } else {
            $last_no_transaction = '001';
        }

        return date('Ym') . str_pad($last_no_transaction + 1, 3, '0', STR_PAD_LEFT);
    }

    private function splitNoTransaction($last_no_transaction)
    {
        return (int) substr($last_no_transaction, -3);
    }
}
