<?php

// Finance => Transaction Type
Route::apiResource('transaction-type', 'TransactionTypeController');

// Finance => Transaction
Route::apiResource('transaction', 'TransactionController')->except('show');
Route::prefix('transaction')->group(function () {
    Route::patch('done/{id}', 'TransactionController@done')->name('transaction.done');
    Route::get('sum_amount', 'TransactionController@sum_amount')->name('transaction.sum.amount');
});