<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Modules\Finance\Models\TransactionType;

class TypeTransaction extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create:typeTransaction';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Membuat Data Type Transaction';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $nama = $this->ask('Name ? ');
        $category = $this->choice('Category [1 = Plus, 2 = Minus] ?', [1 => 1, 2 => 2], false);

        TransactionType::create([
            "name" => $nama,
            "category"  => $category
        ]);

        $this->info("Type Transaction " . $nama . " berhasil dibuat");
    }
}
