<?php
namespace App\Http\Controllers;

Use DB;
use Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\View;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Response;

class StorageController extends Controller
{
    public function file_generate(Request $request, $file)
    {
        $folder = $request->segment(3);
        $storagePath = public_path('storage/'. $folder .'/' . $file);

        return response()->file($storagePath);
    }
}